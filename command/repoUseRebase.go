package command

import (
	"fmt"
	"sort"
	"strings"

	"github.com/urfave/cli"
)

// CmdRepoUseRebase sets a repo to use rebase
func CmdRepoUseRebase(c *cli.Context) error {
	configData, profileName, err := loadProfile(c)
	if err != nil {
		return err
	}

	if c.NArg() != 1 {
		return cli.NewExitError("Usage: \"prp profile repo use-rebase {repoName}\"", 1)
	}

	repoName := c.Args().Get(0)

	profile := configData.Profiles[*profileName]
	repo, repoIndex, err := loadRepo(&profile, repoName)
	if err != nil {
		return err
	}

	if !repo.MergeInsteadOfRebase {
		return cli.NewExitError(fmt.Sprintf("%s already uses rebase", repoName), 1)
	}

	repo.MergeInsteadOfRebase = false
	profile.TrackedRepos[repoIndex] = *repo
	configData.Profiles[*profileName] = profile

	return configData.Write(c.GlobalString("config"))
}

// CompleteRepoUseRebase handles bash autocompletion for the 'profile repo use-rebase' command
func CompleteRepoUseRebase(c *cli.Context) {
	if c.NArg() >= 1 {
		return
	}

	configData, profileName, err := loadProfile(c)
	if err != nil {
		return
	}

	profile := configData.Profiles[*profileName]

	repoNames := []string{}
	for _, repo := range profile.TrackedRepos {
		if repo.MergeInsteadOfRebase {
			repoNames = append(repoNames, fmt.Sprintf("%s/%s", repo.Owner, repo.Name))
		}
	}

	sort.Strings(repoNames)

	fmt.Fprintln(c.App.Writer, strings.Join(repoNames, "\n"))
}
