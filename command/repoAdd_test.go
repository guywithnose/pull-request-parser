package command_test

import (
	"flag"
	"os"
	"testing"

	"github.com/google/go-github/github"
	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/mockServer"
	"gitlab.com/guywithnose/pull-request-parser/command"
	"gitlab.com/guywithnose/pull-request-parser/config"
)

func TestCmdRepoAdd(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo", "bar"}))
	assert.Nil(t, command.CmdRepoAdd(cli.NewContext(nil, set, nil)))
	assert.Nil(t, set.Parse([]string{"own", "reprepreprepreprep"}))
	assert.Nil(t, command.CmdRepoAdd(cli.NewContext(nil, set, nil)))

	modifiedConfigData, err := config.LoadFromFile(configFileName)
	assert.Nil(t, err)

	expectedConfigFile, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	assert.Equal(t, expectedConfigFile, *modifiedConfigData)
}

func TestCmdRepoAddNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	err := command.CmdRepoAdd(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdRepoAddUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := flag.NewFlagSet("test", 0)
	set.String("config", configFileName, "doc")
	set.String("profile", "foo", "doc")
	err := command.CmdRepoAdd(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Usage: \"prp profile repo add {owner} {repoName}\"")
}

func TestCmdRepoAddAlreadyTracked(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own", "reprepreprepreprep"}))
	err := command.CmdRepoAdd(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "own/reprepreprepreprep is already tracked")
}

func TestCompleteRepoAddOwner(t *testing.T) {
	ts := getRepoAddTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "add", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "bar\nown\nsource\n", writer.String())
}

func TestCompleteRepoAddName(t *testing.T) {
	ts := getRepoAddTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"source"}))
	os.Args = []string{"repo", "add", "source", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "reprepreprepreprep\n", writer.String())
}

func TestCompleteRepoAddNameOwnRepos(t *testing.T) {
	ts := getRepoAddTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own"}))
	os.Args = []string{"repo", "add", "own", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo\n", writer.String())
}

func TestCompleteRepoAddNameAlreadyTracked(t *testing.T) {
	ts := getRepoAddTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo"}))
	os.Args = []string{"repo", "add", "foofoofoofoofoofoo", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "\n", writer.String())
}

func TestCompleteRepoAddDone(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo", "bar"}))
	os.Args = []string{"repo", "add", "foofoofoofoofoofoo", "bar", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteRepoAddNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	set.String("profile", "foofoofoofoofoofoo", "doc")
	os.Args = []string{"repo", "add", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteRepoAddReposFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			mockServer.NewRequest("/user/repos?per_page=100", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "add", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "\n", writer.String())
}

func TestCompleteRepoAddRepoFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			userReposRequest,
			userReposRequestPageTwo,
			ownBarReposRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "add", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "bar\nown\n", writer.String())
}

func TestCompleteRepoAddBadApiUrl(t *testing.T) {
	_, configFileName := getConfigWithAPIURL(t, "%s/mockApi")
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "add", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoAdd(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func getRepoAddTestServer(t *testing.T) *mockServer.Server {
	return mockServer.New(
		t,
		[]*mockServer.Request{
			userReposRequest,
			userReposRequestPageTwo,
			ownBarReposRequest,
			ownRepReposRequest,
		},
	)
}

func newRepository(owner, name string, fork bool) *github.Repository {
	return &github.Repository{
		Owner: &github.User{Login: &owner},
		Fork:  &fork,
		Name:  &name,
	}
}

func newRepositoryWithSource(source *github.Repository) *github.Repository {
	owner := "own"
	name := "reprepreprepreprep"
	fork := true
	return &github.Repository{
		Source: source,
		Owner:  &github.User{Login: &owner},
		Fork:   &fork,
		Name:   &name,
	}
}
