package command_test

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/google/go-github/github"
	"gitlab.com/guywithnose/mockServer"
)

var guyUserRequest = mockServer.NewRequest("/user", true).WithResponse(newUser("guy"))
var fooGuyUserRequest = mockServer.NewRequest("/user", true).WithResponse(newUser("fooGuy"))

var pullsPageOneRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/pulls?per_page=100", true).WithResponse([]*github.PullRequest{
	newPullRequest(1, "ABC-123: prOne", "guy", "label", "ref1", "sha1", "baseLabel1", "baseRef1"),
}).WithExtraStep(func(t *testing.T, w http.ResponseWriter, r *http.Request, body []byte, server *mockServer.Server) {
	w.Header().Set(
		"Link",
		fmt.Sprintf(
			`<%s/mockApi/repos/own/reprepreprepreprep/pulls?per_page=100&page=2>; rel="next", `+
				`<%s/mockApi/repos/own/reprepreprepreprep/pulls?per_page=100&page=2>; rel="last"`,
			server.URL,
			server.URL,
		),
	)
})
var pullsPageTwoRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/pulls?page=2&per_page=100", true).WithResponse([]*github.PullRequest{
	newPullRequest(2, "ReallyLong-1 - Pull Request Title", "guy2", "label", "ref2", "sha2", "baseLabel2", "baseRef2"),
})

var ownRepCompareOneRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/compare/label...baseLabel1", true).WithResponse(newCommitsComparison(1))
var ownRepCompareTwoRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/compare/label...baseLabel2", true).WithResponse(newCommitsComparison(0))

var fooBarPullsRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/pulls?per_page=100", true).WithResponse([]*github.PullRequest{
	newPullRequest(1, "fooPrOne", "fooGuy", "fooLabel", "fooRef-1", "fooSha1", "fooBaseLabel1", "fooBaseRef1"),
	newPullRequest(2, "fooPrTwo", "fooGuy", "fooLabel", "fooRef2", "fooSha2", "fooBaseLabel2", "fooBaseRef2"),
	newPullRequest(3, "fooPrThree", "fooGuy", "fooLabel", "fooRef3", "fooSha3", "fooBaseLabel3", "fooBaseRef3"),
})

var fooBarCompareOneRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/compare/fooLabel...fooBaseLabel1",
	true,
).WithResponse(newCommitsComparison(0))
var fooBarCompareTwoRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/compare/fooLabel...fooBaseLabel2",
	true,
).WithResponse(newCommitsComparison(1))
var fooBarCompareThreeRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/compare/fooLabel...fooBaseLabel3",
	true,
).WithResponse(newCommitsComparison(0))

var userReposRequest = mockServer.NewRequest("/user/repos?per_page=100", true).WithResponse([]*github.Repository{
	newRepository("own", "reprepreprepreprep", true),
	newRepository("own", "bar", true),
	newRepository("own", "foofoofoofoofoofoo", false),
	newRepository("bar", "goo", false),
}).WithExtraStep(func(t *testing.T, w http.ResponseWriter, r *http.Request, body []byte, server *mockServer.Server) {
	w.Header().Set(
		"Link",
		fmt.Sprintf(
			`<%s/mockApi/users/own/repos?per_page=100&page=2>; rel="next", `+
				`<%s/mockApi/users/own/repos?per_page=100&page=2>; rel="last"`,
			server.URL,
			server.URL,
		),
	)
})

var userReposRequestPageTwo = mockServer.NewRequest("/user/repos?page=2&per_page=100", true).WithResponse([]*github.Repository{})

var ownBarReposRequest = mockServer.NewRequest("/repos/own/bar", true).WithResponse(newRepositoryWithSource(newRepository("foofoofoofoofoofoo", "bar", false)))

var ownRepReposRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep",
	true,
).WithResponse(newRepositoryWithSource(newRepository("source", "reprepreprepreprep", false)))

var ownRepReviewersOneRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/pulls/1/requested_reviewers",
	true,
).WithResponse(newReviewerList([]string{"foo", "bar"}))
var ownRepReviewersTwoRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/pulls/2/requested_reviewers",
	true,
).WithResponse(newReviewerList([]string{"goo"}))

var fooBarReviewersOneRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/1/requested_reviewers",
	true,
).WithResponse(newReviewerList([]string{"booger", "baz"}))
var fooBarReviewersTwoRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/2/requested_reviewers",
	true,
).WithResponse(newReviewerList([]string{"foo"}))
var fooBarReviewersThreeRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/3/requested_reviewers",
	true,
).WithResponse(newReviewerList([]string{"foo"}))

var ownRepReviewsPageOneRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/pulls/1/reviews?per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("fooGuy"),
}).WithExtraStep(func(t *testing.T, w http.ResponseWriter, r *http.Request, body []byte, server *mockServer.Server) {
	w.Header().Set(
		"Link",
		fmt.Sprintf(
			`<%s/mockApi/repos/own/reprepreprepreprep/pulls/1/reviews?per_page=100&page=2>; rel="next", `+
				`<%s/mockApi/repos/own/reprepreprepreprep/pulls/1/reviews?per_page=100&page=2>; rel="last"`,
			server.URL,
			server.URL,
		),
	)
})
var ownRepReviewsPageTwoRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/pulls/1/reviews?page=2&per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("fooGuy"),
})
var ownRepReviewsTwoRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/pulls/2/reviews?per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("guy"),
})

var fooBarReviewsOneRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/1/reviews?per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("guy"),
	newReview("own"),
	newReview("guy2"),
	newReview("guy3"),
})
var fooBarReviewsTwoRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/2/reviews?per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("guy"),
})
var fooBarReviewsThreeRequest = mockServer.NewRequest(
	"/repos/foofoofoofoofoofoo/bar/pulls/3/reviews?per_page=100",
	true,
).WithResponse([]*github.PullRequestReview{
	newReview("guy"),
	newReview("own"),
	newReview("guy2"),
	newReview("guy3"),
})

var ownRepLabelsRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/issues/1/labels", true).WithResponse([]*github.Label{
	newLabel("label1"),
})
var ownRepLabelsTwoRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/issues/2/labels", true).WithResponse([]*github.Label{})

var fooBarLabelsRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/1/labels", true).WithResponse([]*github.Label{
	newLabel("label2"),
	newLabel("label3"),
})
var fooBarLabelsTwoRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/2/labels", true).WithResponse([]*github.Label{
	newLabel("label4"),
	newLabel("label5"),
	newLabel("really-long-label"),
})
var fooBarLabelsThreeRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/3/labels", true).WithResponse([]*github.Label{
	newLabel("label4"),
	newLabel("label5"),
	newLabel("really-long-label"),
})

var ownRepCommentPageOneRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/issues/1/comments?per_page=100",
	true,
).WithResponse([]*github.IssueComment{
	newComment("foo", "guy"),
}).WithExtraStep(func(t *testing.T, w http.ResponseWriter, r *http.Request, body []byte, server *mockServer.Server) {
	w.Header().Set(
		"Link",
		fmt.Sprintf(
			`<%s/mockApi/repos/own/reprepreprepreprep/issues/1/comments?per_page=100&page=2>; rel="next", `+
				`<%s/mockApi/repos/own/reprepreprepreprep/issues/1/comments?per_page=100&page=2>; rel="last"`,
			server.URL,
			server.URL,
		),
	)
})
var ownRepCommentPageTwoRequest = mockServer.NewRequest(
	"/repos/own/reprepreprepreprep/issues/1/comments?page=2&per_page=100",
	true,
).WithResponse([]*github.IssueComment{
	newComment(":thumbsup:", "own"),
})

var fooBarCommentRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/1/comments?per_page=100", true).WithResponse([]*github.IssueComment{
	newComment(":+1:", "fooGuy"),
	newComment(":thumbsup:", "guy2"),
	newComment("LGTM", "guy"),
})
var fooBarCommentTwoRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/2/comments?per_page=100", true).WithResponse([]*github.IssueComment{
	newComment("foo", "guy"),
})
var fooBarCommentThreeRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/issues/3/comments?per_page=100", true).WithResponse([]*github.IssueComment{
	newComment("foo", "guy"),
})

var ownRepTwoCommentRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/issues/2/comments?per_page=100", true).WithResponse([]*github.IssueComment{
	newComment(":+1:", "guy"),
	newComment("LGTM", "guy2"),
})

var first, _ = time.Parse("01/02/06", "01/01/06")
var second, _ = time.Parse("01/02/06", "01/02/06")
var ownRepSha1StatusRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/commits/sha1/statuses", true).WithResponse([]*github.RepoStatus{
	newStatus("build1", "success", second),
	newStatus("build1", "pending", first),
})
var ownRepSha2StatusRequest = mockServer.NewRequest("/repos/own/reprepreprepreprep/commits/sha2/statuses", true).WithResponse([]*github.RepoStatus{
	newStatus("build1", "failure", second),
	newStatus("build1", "pending", first),
})
var fooBarSha1StatusRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/commits/fooSha1/statuses", true).WithResponse([]*github.RepoStatus{
	newStatus("build1", "pending", second),
	newStatus("build2", "pending", first),
	newStatus("build2", "success", second),
	newStatus("goo", "pending", first),
	newStatus("goo", "failure", second),
	newStatus("foo", "pending", first),
	newStatus("foo", "failure", second),
})
var fooBarSha2StatusRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/commits/fooSha2/statuses", true).WithResponse([]*github.RepoStatus{})
var fooBarSha3StatusRequest = mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/commits/fooSha3/statuses", true).WithResponse([]*github.RepoStatus{})

var jiraSessionRequest = mockServer.NewRequest(
	"/rest/auth/1/session",
	true,
).WithResponse(`{"session":{"name":"cloud.session.token","value":"12345678901234567890"}}`).WithMulipleCalls()

var jiraIssueAbc123Request = mockServer.NewRequest("/rest/api/2/issue/ABC-123", true).WithResponse(
	`{
  "key": "ABC-123",
  "fields": {
            "status": {
              "name": "Review"
            },
			"assignee": {
				"displayName": "Dude"
			}
		}
}`)

var jiraIssueFooRefRequest = mockServer.NewRequest("/rest/api/2/issue/fooRef-1", true).WithResponse(`{
  "key": "fooRef-1",
  "fields": {
            "status": {
              "name": "Pass"
            },
			"assignee": {
				"displayName": "Looooooooooooooooooong Naaaaaaaaame"
			}
		}
}`)
var jiraIssueReallyLongRequest = mockServer.NewRequest("/rest/api/2/issue/ReallyLong-1", true).WithResponse(`{
  "key": "ReallyLong-1",
  "fields": {
            "status": {
              "name": "Open"
            },
			"assignee": {
				"displayName": "Guy"
			}
		}
}`)
var yourlsRequest = mockServer.NewRequest("/yourls-api.php", true).WithMulipleCalls().WithResponse(`{
	"shortUrl": "shortUrl"
}`)
var yourlsRequestError = mockServer.NewRequest("/yourls-api.php", true).WithMulipleCalls().WithExtraStep(func(t *testing.T, w http.ResponseWriter, r *http.Request, body []byte, server *mockServer.Server) {
	w.Header().Set("Location", "/:/")
})
var yourlsRequestBadJson = mockServer.NewRequest("/yourls-api.php", true).WithMulipleCalls().WithResponse(`{,}`)

func newUser(login string) *github.User {
	return &github.User{Login: &login}
}

func newPullRequest(number int, title, owner, label, ref, sha, baseLabel, baseRef string) *github.PullRequest {
	headSSHURL := fmt.Sprintf("%sSSHURL", label)
	baseSSHURL := fmt.Sprintf("%sSSHURL", baseLabel)
	htmlUrl := "htmlUrl"
	return &github.PullRequest{
		Number:  &number,
		Title:   &title,
		HTMLURL: &htmlUrl,
		Head: &github.PullRequestBranch{
			Label: &label,
			Ref:   &ref,
			SHA:   &sha,
			User:  &github.User{Login: &owner},
			Repo:  &github.Repository{SSHURL: &headSSHURL},
		},
		Base: &github.PullRequestBranch{
			Label: &baseLabel,
			Ref:   &baseRef,
			Repo:  &github.Repository{SSHURL: &baseSSHURL},
		},
	}
}

func newCommitsComparison(aheadBy int) *github.CommitsComparison {
	return &github.CommitsComparison{
		AheadBy: &aheadBy,
	}
}

func newReviewerList(reviewers []string) *github.Reviewers {
	teams := []*github.Team{}
	for index := range reviewers {
		teams = append(teams, &github.Team{Name: &reviewers[index]})
	}

	return &github.Reviewers{Teams: teams}
}

func newComment(body, user string) *github.IssueComment {
	return &github.IssueComment{
		Body: &body,
		User: newUser(user),
	}
}

func newReview(user string) *github.PullRequestReview {
	state := "APPROVED"
	return &github.PullRequestReview{
		User:  newUser(user),
		State: &state,
	}
}

func newLabel(name string) *github.Label {
	return &github.Label{
		Name: &name,
	}
}

func newStatus(context, state string, created time.Time) *github.RepoStatus {
	return &github.RepoStatus{
		Context:   &context,
		State:     &state,
		CreatedAt: &created,
	}
}
