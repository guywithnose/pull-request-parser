package command_test

import (
	"flag"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/pull-request-parser/command"
)

func TestCmdRepoIgnoreBuild(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo/bar", "goo"}))
	assert.Nil(t, command.CmdRepoIgnoreBuild(cli.NewContext(nil, set, nil)))

	expectedConfigFile, disposableConfigFile := getConfigWithIgnoredBuild(t)
	removeFile(t, disposableConfigFile)
	assertConfigFile(t, expectedConfigFile, configFileName)
}

func TestCmdRepoIgnoreBuildNoConfig(t *testing.T) {
	err := command.CmdRepoIgnoreBuild(cli.NewContext(nil, flag.NewFlagSet("test", 0), nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdRepoIgnoreBuildInvalidRepo(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "goo"}))

	err := command.CmdRepoIgnoreBuild(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Not a valid Repo: own/reprepreprepreprep")
}

func TestCmdRepoIgnoreBuildUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	err := command.CmdRepoIgnoreBuild(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Usage: \"prp profile repo ignore-build {repoName} {buildName}\"")
}

func TestCmdRepoIgnoreBuildAlreadyIgnored(t *testing.T) {
	_, configFileName := getConfigWithIgnoredBuild(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo/bar", "goo"}))
	err := command.CmdRepoIgnoreBuild(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "goo is already being ignored by foofoofoofoofoofoo/bar")
}

func TestCompleteRepoIgnoreBuildRepos(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "ignore-build", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoIgnoreBuild(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo/bar\nown/reprepreprepreprep\n", writer.String())
}

func TestCompleteRepoIgnoreBuildIgnoredBuilds(t *testing.T) {
	_, configFileName := getConfigWithIgnoredBuild(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep"}))
	os.Args = []string{"repo", "ignore-build", "own/reprepreprepreprep", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoIgnoreBuild(cli.NewContext(app, set, nil))
	assert.Equal(t, "goo\n", writer.String())
}

func TestCompleteRepoIgnoreBuildDone(t *testing.T) {
	_, configFileName := getConfigWithIgnoredBuild(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "goo"}))
	os.Args = []string{"repo", "ignore-build", "own/reprepreprepreprep", "goo", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoIgnoreBuild(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteRepoIgnoreBuildNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	os.Args = []string{"repo", "ignore-build", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoIgnoreBuild(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}
