package command_test

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/pull-request-parser/config"
)

func removeFile(t *testing.T, fileName string) {
	t.Helper()
	assert.Nil(t, os.RemoveAll(fileName))
}

func getConfigWithFooProfile(t *testing.T) (config.PrpConfig, string) {
	t.Helper()
	conf := config.PrpConfig{
		Profiles: map[string]config.Profile{
			"foo": {
				TrackedRepos: []config.Repo{},
			},
		},
	}

	configFile, err := ioutil.TempFile("/tmp", "config")
	assert.Nil(t, err)
	assert.Nil(t, conf.Write(configFile.Name()))
	return conf, configFile.Name()
}

func getConfigWithTwoRepos(t *testing.T) (config.PrpConfig, string) {
	t.Helper()
	conf := config.PrpConfig{
		Profiles: map[string]config.Profile{
			"foo": {
				TrackedRepos: []config.Repo{
					{
						Owner:         "foofoofoofoofoofoo",
						Name:          "bar",
						IgnoredBuilds: []string{},
					},
					{
						Owner:         "own",
						Name:          "reprepreprepreprep",
						IgnoredBuilds: []string{},
					},
				},
			},
		},
	}

	configFile, err := ioutil.TempFile("/tmp", "config")
	assert.Nil(t, err)
	assert.Nil(t, conf.Write(configFile.Name()))
	return conf, configFile.Name()
}

func getConfigWithIgnoredBuild(t *testing.T) (config.PrpConfig, string) {
	t.Helper()
	conf, configFileName := getConfigWithTwoRepos(t)
	profile := conf.Profiles["foo"]
	profile.TrackedRepos[0].IgnoredBuilds = []string{"goo"}
	conf.Profiles["foo"] = profile
	assert.Nil(t, conf.Write(configFileName))
	return conf, configFileName
}

func getConfigWithMerge(t *testing.T) (config.PrpConfig, string) {
	t.Helper()
	conf, configFileName := getConfigWithTwoRepos(t)
	profile := conf.Profiles["foo"]
	profile.TrackedRepos[0].MergeInsteadOfRebase = true
	conf.Profiles["foo"] = profile
	assert.Nil(t, conf.Write(configFileName))
	return conf, configFileName
}

func appWithTestWriters() (*cli.App, *bytes.Buffer, *bytes.Buffer) {
	app := cli.NewApp()
	writer := new(bytes.Buffer)
	errWriter := new(bytes.Buffer)
	app.Writer = writer
	app.ErrWriter = errWriter
	return app, writer, errWriter
}

func getBaseFlagSet(configFileName string) *flag.FlagSet {
	set := flag.NewFlagSet("test", 0)
	set.String("config", configFileName, "doc")
	set.String("profile", "foo", "doc")
	return set
}

func assertConfigFile(t *testing.T, expectedConfigFile config.PrpConfig, configFileName string) {
	t.Helper()
	modifiedConfigData, err := config.LoadFromFile(configFileName)
	assert.Nil(t, err)
	assert.Equal(t, *modifiedConfigData, expectedConfigFile)
}

func getConfigWithAPIURL(t *testing.T, url string) (config.PrpConfig, string) {
	t.Helper()
	conf, configFileName := getConfigWithIgnoredBuild(t)
	profile := conf.Profiles["foo"]
	profile.APIURL = fmt.Sprintf("%s/", url)
	profile.Token = "abc"
	conf.Profiles["foo"] = profile
	assert.Nil(t, conf.Write(configFileName))
	return conf, configFileName
}

func getConfigWithAPIURLAndPath(t *testing.T, url, path string) (config.PrpConfig, string) {
	t.Helper()
	conf, configFileName := getConfigWithAPIURL(t, url)
	profile := conf.Profiles["foo"]
	profile.TrackedRepos[1].LocalPath = path
	conf.Profiles["foo"] = profile
	assert.Nil(t, conf.Write(configFileName))
	return conf, configFileName
}

func getConfigWithAPIURLAndPathUseMerge(t *testing.T, url, path string) (config.PrpConfig, string) {
	t.Helper()
	conf, configFileName := getConfigWithAPIURLAndPath(t, url, path)
	profile := conf.Profiles["foo"]
	profile.TrackedRepos[0].MergeInsteadOfRebase = true
	profile.TrackedRepos[1].MergeInsteadOfRebase = true
	conf.Profiles["foo"] = profile
	assert.Nil(t, conf.Write(configFileName))
	return conf, configFileName
}
