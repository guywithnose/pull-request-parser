package command_test

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/mockServer"
	"gitlab.com/guywithnose/pull-request-parser/command"
	"gitlab.com/guywithnose/runner"
)

func TestCmdAutoRebase(t *testing.T) {
	repoDir := fmt.Sprintf("%s/repo", os.TempDir())
	var testCases = []struct {
		name             string
		expectedCommands []*runner.ExpectedCommand
		output           []string
		verbose          bool
		expectedError    bool
		useMerge         bool
		dryRun           bool
	}{
		{
			"Normal",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
			},
			[]string{""},
			false,
			false,
			false,
			false,
		},
		{
			"NormalMerge",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git merge upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
			},
			[]string{""},
			false,
			false,
			true,
			false,
		},
		{
			"Verbose",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"VerboseMerge",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git merge upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Merging with upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"",
			},
			true,
			false,
			true,
			false,
		},
		{
			"LocalChanges",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"FailureDetectingChanges",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", -1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				fmt.Sprintf("could not rebase PR #1 in own/reprepreprepreprep because: unable to detect local changes in %s", repoDir),
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"AnalyzeOwnedRemoteNotFound",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "upstream\tbaseLabel1SSHURL (fetch)", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"could not rebase PR #1 in own/reprepreprepreprep because: no remote exists in /tmp/repo that points to labelSSHURL",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"AnalyzeUpstreamRemoteNotFound",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"could not rebase PR #1 in own/reprepreprepreprep because: no remote exists in /tmp/repo that points to baseLabel1SSHURL",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"AnalyzeRemotesFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "failure analyzing remotes", 1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to analyze remotes in /tmp/repo",
				"failure analyzing remotes",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"RestoreOriginalBranchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "checkout failure", 1),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"",
				"checkout failure",
				"Warning: could not go back to branch currentBranch in /tmp/repo",
				"Popping the stash",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"DeleteTempBranchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "delete branch failure", 1),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"",
				"delete branch failure",
				"Warning: could not delete temporary branch prp-ref1 in /tmp/repo",
				"Popping the stash",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"StashPopFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "stash pop failure", 1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"",
				"stash pop failure",
				"Warning: could not pop stash in /tmp/repo",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"PushFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "push failure", 1),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to push to origin/ref1",
				"push failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"StashFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "stash error", 1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				fmt.Sprintf("could not rebase PR #1 in own/reprepreprepreprep because: unable to stash changes in %s", repoDir),
				"stash error",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"GetCurrentBranchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "Invalid output", 1),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Popping the stash",
				fmt.Sprintf("could not rebase PR #1 in own/reprepreprepreprep because: unable to get current branch name in %s", repoDir),
				"exit status 1",
				"Invalid output",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"NoBranchCheckedOut",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "Not a branch", 128),
				runner.NewExpectedCommand(repoDir, "git rev-parse HEAD", "abcdefg", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git push origin prp-ref1:ref1 --force", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout abcdefg", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is abcdefg",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Pushing to origin/ref1",
				"Going back to branch abcdefg",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"",
			},
			true,
			false,
			false,
			false,
		},
		{
			"NoValidHead",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "Not a branch", 128),
				runner.NewExpectedCommand(repoDir, "git rev-parse HEAD", "Not a branch", 128),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Popping the stash",
				fmt.Sprintf("could not rebase PR #1 in own/reprepreprepreprep because: unable to get current branch name in %s", repoDir),
				"no branch checked out in /tmp/repo",
				"Not a branch",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"TempBranchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "checkout error", 1),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Popping the stash",
				fmt.Sprintf("could not rebase PR #1 in own/reprepreprepreprep because: unable to checkout temporary branch prp-ref1 in %s", repoDir),
				"checkout error",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"TempBranchExists",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "branch exists", 128),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: branch prp-ref1 already exists",
				"branch exists",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"ResetFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "reset failure", 1),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to reset the code to origin/ref1",
				"reset failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"RebaseFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "rebase failure", 1),
				runner.NewExpectedCommand(repoDir, "git rebase --abort", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to rebase ref1 against upstream/baseRef1 in own/reprepreprepreprep," +
					" there may be a conflict",
				"rebase failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"MergeFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git merge upstream/baseRef1", "merge failure", 1),
				runner.NewExpectedCommand(repoDir, "git merge --abort", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Merging with upstream/baseRef1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to merge upstream/baseRef1 into ref1 in own/reprepreprepreprep, there may be a conflict",
				"merge failure",
				"",
			},
			true,
			true,
			true,
			false,
		},
		{
			"RebaseFailureAbortFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "rebase failure", 1),
				runner.NewExpectedCommand(repoDir, "git rebase --abort", "", 1),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Rebasing against upstream/baseRef1",
				"could not abort rebase PR #1 in own/reprepreprepreprep because: exit status 1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to rebase ref1 against upstream/baseRef1 in own/reprepreprepreprep," +
					" there may be a conflict",
				"rebase failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"MergeFailureAbortFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git merge upstream/baseRef1", "merge failure", 1),
				runner.NewExpectedCommand(repoDir, "git merge --abort", "", 1),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git stash pop", "", 0),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"Local changes found... stashing",
				"Saving current branch name",
				"Current branch name is currentBranch",
				"Checking out temporary branch: prp-ref1",
				"Resetting code to origin/ref1",
				"Merging with upstream/baseRef1",
				"could not abort merge PR #1 in own/reprepreprepreprep because: exit status 1",
				"Going back to branch currentBranch",
				"Deleting temporary branch prp-ref1",
				"Popping the stash",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to merge upstream/baseRef1 into ref1 in own/reprepreprepreprep, there may be a conflict",
				"merge failure",
				"",
			},
			true,
			true,
			true,
			false,
		},
		{
			"OwnedRemoteFetchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "fetch failure", 1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to fetch code from origin",
				"fetch failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"UpstreamRemoteFetchFailure",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 1),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "fetch failure", 1),
			},
			[]string{
				"Requesting repo data from config",
				"Analyzing remotes",
				"Checking for local changes",
				"Fetching from remote: origin",
				"Fetching from remote: upstream",
				"could not rebase PR #1 in own/reprepreprepreprep because: unable to fetch code from upstream",
				"fetch failure",
				"",
			},
			true,
			true,
			false,
			false,
		},
		{
			"DryRun",
			[]*runner.ExpectedCommand{
				runner.NewExpectedCommand(repoDir, "git remote -v", "origin\tlabelSSHURL (push)\nupstream\tbaseLabel1SSHURL (fetch)", 0),
				runner.NewExpectedCommand(repoDir, "git diff-index --quiet HEAD", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch origin", "", 0),
				runner.NewExpectedCommand(repoDir, "git fetch upstream", "", 0),
				runner.NewExpectedCommand(repoDir, "git symbolic-ref HEAD", "currentBranch", 0),
				runner.NewExpectedCommand(repoDir, "git checkout -b prp-ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git reset --hard origin/ref1", "", 0),
				runner.NewExpectedCommand(repoDir, "git rebase upstream/baseRef1", "", 0),
				runner.NewExpectedCommand(repoDir, "git checkout currentBranch", "", 0),
				runner.NewExpectedCommand(repoDir, "git branch -D prp-ref1", "", 0),
			},
			[]string{""},
			false,
			false,
			false,
			true,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			runner := &runner.Test{ExpectedCommands: tc.expectedCommands}
			writer := runBaseCommand(t, repoDir, runner, tc.verbose, tc.expectedError, tc.useMerge, tc.dryRun)
			assert.Equal(t, tc.output, strings.Split(writer.String(), "\n"))
			removeFile(t, repoDir)
		})
	}
}

func TestCmdAutoRebasePullRequestNumber(t *testing.T) {
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	repoDir := fmt.Sprintf("%s/repo", os.TempDir())
	assert.Nil(t, os.MkdirAll(fmt.Sprintf("%s/.git", repoDir), 0777))
	defer removeFile(t, repoDir)
	_, configFileName := getConfigWithAPIURLAndPath(t, ts.URL, repoDir)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.Int("pull-request-number", 2, "doc")
	app, writer, _ := appWithTestWriters()
	cb := &runner.Test{ExpectedCommands: []*runner.ExpectedCommand{}}
	assert.Nil(t, command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil)))
	assert.Equal(t, []*runner.ExpectedCommand{}, cb.ExpectedCommands)
	assert.Equal(t, []error(nil), cb.Errors)
	assert.Equal(t, "", writer.String())
}

func TestCmdAutoRebaseBadAPIURL(t *testing.T) {
	_, configFileName := getConfigWithAPIURL(t, "%s/mockApi")
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app := cli.NewApp()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, `parse "%s/mockApi/": invalid URL escape "%s/"`)
}

func TestCmdAutoRebaseUserFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			mockServer.NewRequest("/user", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app := cli.NewApp()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, fmt.Sprintf("GET %s/user: 500  []", ts.URL))
}

func TestCmdAutoRebaseUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foo"}))
	app := cli.NewApp()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "Usage: \"prp auto-rebase\"")
}

func TestCmdAutoRebaseNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	app := cli.NewApp()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdAutoRebaseNoPath(t *testing.T) {
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, _, writer := appWithTestWriters()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.Nil(t, err)
	assert.Equal(t, "could not rebase PR #1 in own/reprepreprepreprep because: path was not set for repo: own/reprepreprepreprep\n", writer.String())
}

func TestCmdAutoRebaseInvalidPath(t *testing.T) {
	repoDir := fmt.Sprintf("%s/repo", os.TempDir())
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURLAndPath(t, ts.URL, repoDir)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, _, writer := appWithTestWriters()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "unable to rebase all pull requests")
	assert.Equal(t, "could not rebase PR #1 in own/reprepreprepreprep because: path does not exist: /tmp/repo\n", writer.String())
}

func TestCmdAutoRebaseNonGitPath(t *testing.T) {
	repoDir := fmt.Sprintf("%s/repo", os.TempDir())
	assert.Nil(t, os.MkdirAll(repoDir, 0777))
	defer removeFile(t, repoDir)
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURLAndPath(t, ts.URL, repoDir)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, _, writer := appWithTestWriters()
	cb := &runner.Test{}
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "unable to rebase all pull requests")
	assert.Equal(t, "could not rebase PR #1 in own/reprepreprepreprep because: path is not a git repo: /tmp/repo\n", writer.String())
}

func TestCompleteAutoRebaseFlags(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	app.Commands = []cli.Command{
		{
			Name: "auto-rebase",
			Flags: []cli.Flag{
				cli.StringSliceFlag{Name: "repo, r"},
				cli.IntFlag{Name: "pull-request-number, prNum, n"},
				cli.BoolFlag{Name: "verbose, v"},
			},
		},
	}
	os.Args = []string{"auto-rebase", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "--repo\n--pull-request-number\n--verbose\n", writer.String())
}

func TestCompleteAutoRebaseRepo(t *testing.T) {
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"auto-rebase", "--repo", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "own/reprepreprepreprep\n", writer.String())
}

func TestCompleteAutoRebaseRepoMulti(t *testing.T) {
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	repoFlag := cli.StringSlice{"own/reprepreprepreprep"}
	set.Var(&repoFlag, "repo", "doc")
	app, _, writer := appWithTestWriters()
	os.Args = []string{"auto-rebase", "--repo", "own/reprepreprepreprep", "--repo", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteAutoRebasePullRequestNumber(t *testing.T) {
	ts := getAutoRebaseTestServer(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"auto-rebase", "--pull-request-number", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "1\n", writer.String())
}

func TestCompleteAutoRebaseNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"auto-rebase", "--pull-request-number", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteAutoRebaseBadAPIURL(t *testing.T) {
	_, configFileName := getConfigWithAPIURL(t, "%s/mockApi")
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"auto-rebase", "--pull-request-number", "--completion"}
	command.CompleteAutoRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func getAutoRebaseTestServer(t *testing.T) *mockServer.Server {
	return mockServer.New(
		t,
		[]*mockServer.Request{
			guyUserRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			fooBarPullsRequest,
			ownRepCompareOneRequest,
		},
	)
}

func runBaseCommand(t *testing.T, repoDir string, cb *runner.Test, verbose, expectedError, useMerge, dryRun bool) *bytes.Buffer {
	t.Helper()
	assert.Nil(t, os.MkdirAll(fmt.Sprintf("%s/.git", repoDir), 0777))
	ts := getAutoRebaseTestServer(t)
	var configFileName string
	if useMerge {
		_, configFileName = getConfigWithAPIURLAndPathUseMerge(t, ts.URL, repoDir)
	} else {
		_, configFileName = getConfigWithAPIURLAndPath(t, ts.URL, repoDir)
	}

	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.Bool("verbose", verbose, "doc")
	set.Bool("dry-run", dryRun, "doc")
	app, _, writer := appWithTestWriters()
	err := command.CmdAutoRebase(cb)(cli.NewContext(app, set, nil))
	ts.Close()
	if expectedError {
		assert.EqualError(t, err, "unable to rebase all pull requests")
	} else {
		assert.Nil(t, err)
	}

	assert.Equal(t, []*runner.ExpectedCommand{}, cb.ExpectedCommands)
	assert.Equal(t, []error(nil), cb.Errors)
	return writer
}

func TestHelperProcess(*testing.T) {
	runner.ErrorCodeHelper()
}
