package command

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"text/tabwriter"

	"github.com/reconquest/loreley"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/pull-request-parser/config"
)

func sortRepoNames(profile *config.Profile) []string {
	repoNames := []string{}
	for _, repo := range profile.TrackedRepos {
		repoNames = append(repoNames, fmt.Sprintf("%s/%s", repo.Owner, repo.Name))
	}

	sort.Strings(repoNames)
	return repoNames
}

func unique(in []string) []string {
	set := make(map[string]bool, len(in))
	for _, value := range in {
		set[value] = true
	}

	out := make([]string, 0, len(set))
	for value := range set {
		out = append(out, value)
	}

	return out
}

func shortenLabel(label string) string {
	parts := strings.Split(label, "-")
	initials := []string{}
	for _, part := range parts {
		initials = append(initials, strings.ToUpper(string(part[0])))
	}

	return strings.Join(initials, "")
}

func printResults(prs <-chan *pullRequest, verbose bool, w io.Writer) error {
	buffer := &bytes.Buffer{}
	tabW := tabwriter.NewWriter(buffer, 0, 0, 0, ' ', tabwriter.Debug|tabwriter.FilterHTML)
	fmt.Fprintln(tabW, "Repo\tID\tTitle\tOwner\tBranch\tTarget\t+1\tUTD\tStatus\tReview\tLabels\tReviewers\tJira\tStatus\tAssigned\tLink")
	count := 0
	for pr := range prs {
		printResult(pr, verbose, tabW)
		count++
	}

	_ = tabW.Flush()

	output := parseColors(buffer.String())

	fmt.Fprint(w, output)
	fmt.Fprintf(w, "Total %d\n", count)

	return nil
}

func printResult(pr *pullRequest, verbose bool, writer io.Writer) {
	title := pr.Title
	if !verbose {
		title = fmt.Sprintf("%.10s", title)
	}

	labels := strings.Join(pr.Labels, ",")
	if !verbose {
		shortLabels := []string{}
		for _, label := range pr.Labels {
			shortLabels = append(shortLabels, shortenLabel(label))
		}

		labels = strings.Join(shortLabels, ",")
	}

	repoName := fmt.Sprintf("%s/%s", pr.Repo.Owner, pr.Repo.Name)
	if !verbose && len(repoName) > 20 {
		name := pr.Repo.Name
		if len(name) > 15 {
			name = name[:15]
		}

		repoName = fmt.Sprintf("%s/%s", pr.Repo.Owner[:2], name)
	}

	owner := pr.Owner
	if !verbose && len(owner) > 5 {
		owner = owner[:5]
	}

	branch := pr.Branch
	if !verbose && len(branch) > 6 {
		branch = branch[:6]
	}

	assignee := pr.JiraData.Assignee
	jiraId := pr.JiraData.ID
	if !verbose {
		names := strings.Split(assignee, " ")
		firstName := names[0]
		lastName := ""
		if len(names) > 1 {
			lastName = names[1]
			if len(lastName) > 1 {
				lastName = lastName[:1]
			}
		}
		if len(firstName) > 6 {
			firstName = firstName[:6]
		}

		assignee = fmt.Sprintf("%s %s", firstName, lastName)

		jiraIdParts := strings.Split(jiraId, "-")
		if len(jiraIdParts) == 2 && len(jiraIdParts[0]) > 3 {
			jiraIdParts[0] = jiraIdParts[0][:3]
			jiraId = strings.Join(jiraIdParts, "-")
		}
	}

	fmt.Fprintf(
		writer,
		"%s%s\t%d\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s<reset>\n",
		pr.Color,
		repoName,
		pr.PullRequestID,
		strings.Replace(title, "&", "and", -1),
		owner,
		branch,
		pr.TargetBranch,
		strconv.Itoa(pr.Approvals),
		boolToString(pr.Rebased),
		buildStatus(pr.BuildInfo, verbose),
		boolToString(pr.NeedsMyApproval),
		labels,
		buildReviewers(pr.Reviewers, verbose),
		jiraId,
		pr.JiraData.Status,
		assignee,
		pr.Link,
	)
}

func parseColors(output string) string {
	loreley.DelimLeft = "<"
	loreley.DelimRight = ">"
	result, _ := loreley.CompileAndExecuteToString(output, nil, nil)
	return result
}

func buildStatus(contexts map[string]string, verbose bool) string {
	keys := make([]string, 0, len(contexts))
	for key := range contexts {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	if verbose {
		var status []string
		for _, key := range keys {
			status = append(status, fmt.Sprintf("%s:%s", key, contexts[key]))
		}

		return strings.Join(status, "/")
	}

	counts := map[string]int{}
	for _, key := range keys {
		if contexts[key] == "success" {
			counts["Y"]++
		} else if contexts[key] == "pending" {
			counts["?"]++
		} else {
			counts["N"]++
		}
	}

	countStrings := []string{}
	for _, status := range []string{"Y", "N", "?"} {
		if _, exists := counts[status]; exists {
			countStrings = append(countStrings, fmt.Sprintf("%d%s", counts[status], status))
		}
	}

	return strings.Join(countStrings, "/")
}

func buildReviewers(reviewers []string, verbose bool) string {
	if !verbose {
		for index, reviewer := range reviewers {
			reviewers[index] = reviewer[:3]
		}
	}

	return strings.Join(reviewers, ",")
}

func boolToString(status bool) string {
	if status {
		return "Y"
	}

	return "N"
}

func filterPullRequestsByRepo(prs <-chan *pullRequest, owner string, repos []string) <-chan *pullRequest {
	filteredPullRequests := make(chan *pullRequest, 10)
	go func() {
		for pr := range prs {
			if pr.matchesRepoFilter(owner, repos) {
				filteredPullRequests <- pr
			}
		}

		close(filteredPullRequests)
	}()
	return filteredPullRequests
}

func stringSliceContains(needle string, haystack []string) bool {
	for _, straw := range haystack {
		if needle == straw {
			return true
		}
	}

	return false
}

func checkPath(localPath string) error {
	if _, err := os.Stat(localPath); os.IsNotExist(err) {
		return cli.NewExitError(fmt.Sprintf("path does not exist: %s", localPath), 1)
	}

	if _, err := os.Stat(fmt.Sprintf("%s/.git", localPath)); os.IsNotExist(err) {
		return cli.NewExitError(fmt.Sprintf("path is not a git repo: %s", localPath), 1)
	}

	return nil
}
