package command_test

import (
	"flag"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/pull-request-parser/command"
	"gitlab.com/guywithnose/pull-request-parser/config"
)

func TestCmdRepoUseMergeRebase(t *testing.T) {
	rebaseConfig, rebaseConfigFileName := getConfigWithMerge(t)
	defer removeFile(t, rebaseConfigFileName)
	mergeConfig, mergeConfigFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, mergeConfigFileName)
	baseTest(t, rebaseConfigFileName, mergeConfig, command.CmdRepoUseRebase)
	baseTest(t, mergeConfigFileName, rebaseConfig, command.CmdRepoUseMerge)
}

func baseTest(t *testing.T, configFileName string, expectedConfig config.PrpConfig, f func(*cli.Context) error) {
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo/bar"}))
	assert.Nil(t, f(cli.NewContext(nil, set, nil)))

	assertConfigFile(t, expectedConfig, configFileName)
}

func TestCmdRepoUseMergeRebaseNoConfig(t *testing.T) {
	err := command.CmdRepoUseRebase(cli.NewContext(nil, flag.NewFlagSet("test", 0), nil))
	assert.EqualError(t, err, "You must specify a config file")
	err = command.CmdRepoUseMerge(cli.NewContext(nil, flag.NewFlagSet("test", 0), nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdRepoUseMergeRebaseInvalidRepo(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep"}))

	err := command.CmdRepoUseRebase(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Not a valid Repo: own/reprepreprepreprep")
	err = command.CmdRepoUseMerge(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Not a valid Repo: own/reprepreprepreprep")
}

func TestCmdRepoUseMergeRebaseUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	err := command.CmdRepoUseRebase(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Usage: \"prp profile repo use-rebase {repoName}\"")
	err = command.CmdRepoUseMerge(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Usage: \"prp profile repo use-merge {repoName}\"")
}

func TestCmdRepoUseRebaseAlreadyRebasing(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo/bar"}))
	err := command.CmdRepoUseRebase(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "foofoofoofoofoofoo/bar already uses rebase")
}

func TestCmdRepoUseMergeAlreadyMerging(t *testing.T) {
	_, configFileName := getConfigWithMerge(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foofoofoofoofoofoo/bar"}))
	err := command.CmdRepoUseMerge(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "foofoofoofoofoofoo/bar already uses merge instead of rebase")
}

func TestCompleteRepoUseRebaseRepos(t *testing.T) {
	_, configFileName := getConfigWithMerge(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoUseRebase(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo/bar\n", writer.String())
}

func TestCompleteRepoUseMergeRepos(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoUseMerge(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo/bar\nown/reprepreprepreprep\n", writer.String())
}

func TestCompleteRepoUseMergeRebaseDone(t *testing.T) {
	_, configFileName := getConfigWithMerge(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "goo"}))
	os.Args = []string{"repo", "", "own/reprepreprepreprep", "goo", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoUseRebase(cli.NewContext(app, set, nil))
	command.CompleteRepoUseMerge(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteRepoUseMergeRebaseNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	os.Args = []string{"repo", "", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoUseRebase(cli.NewContext(app, set, nil))
	command.CompleteRepoUseMerge(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}
