package command_test

import (
	"flag"
	"fmt"
	"os"
	"sort"
	"strings"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/mockServer"
	"gitlab.com/guywithnose/pull-request-parser/command"
)

func TestCmdParse(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseVerbose(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.Bool("verbose", true, "doc")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo                  |ID|Title                            |Owner |Branch  |Target     |+1|UTD|Status                                   |Review|" +
				"Labels                         |Reviewers |Jira|Status|Assigned|Link",
			"foofoofoofoofoofoo/bar|1 |fooPrOne                         |fooGuy|fooRef-1|fooBaseRef1|5 |Y  |build1:pending/build2:success/foo:failure|N     |" +
				"label2,label3                  |booger,baz|    |      |        |htmlUrl",
			"foofoofoofoofoofoo/bar|2 |fooPrTwo                         |fooGuy|fooRef2 |fooBaseRef2|1 |N  |                                         |N     |" +
				"label4,label5,really-long-label|foo       |    |      |        |htmlUrl",
			"foofoofoofoofoofoo/bar|3 |fooPrThree                       |fooGuy|fooRef3 |fooBaseRef3|4 |Y  |                                         |N     |" +
				"label4,label5,really-long-label|foo       |    |      |        |htmlUrl",
			"own/reprepreprepreprep|1 |ABC-123: prOne                   |guy   |ref1    |baseRef1   |2 |N  |build1:success                           |N     |" +
				"label1                         |foo,bar   |    |      |        |htmlUrl",
			"own/reprepreprepreprep|2 |ReallyLong-1 - Pull Request Title|guy2  |ref2    |baseRef2   |2 |Y  |build1:failure                           |Y     |" +
				"                               |goo       |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseNeedRebase(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.Bool("need-rebase", true, "doc")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:3])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status|Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |      |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y    |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"Total 2",
			"",
		},
		output,
	)
}

func TestCmdParseUserFilter(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarPullsRequest,
			fooGuyUserRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepCompareOneRequest,
			ownRepLabelsRequest,
			ownRepReviewersOneRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepSha1StatusRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.String("owner", "guy", "doc")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:2])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target  |+1|UTD|Status|Review|Labels|Reviewers|Jira|Status|Assigned|Link",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1|2 |N  |1Y    |N     |L     |foo,bar  |    |      |        |htmlUrl",
			"Total 1",
			"",
		},
		output,
	)
}

func TestCmdParseRepoFilter(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{
		fooBarCommentRequest,
		fooBarCommentThreeRequest,
		fooBarCommentTwoRequest,
		fooBarCompareOneRequest,
		fooBarCompareThreeRequest,
		fooBarCompareTwoRequest,
		fooBarLabelsRequest,
		fooBarLabelsThreeRequest,
		fooBarLabelsTwoRequest,
		fooBarPullsRequest,
		fooBarReviewersOneRequest,
		fooBarReviewersThreeRequest,
		fooBarReviewersTwoRequest,
		fooBarReviewsOneRequest,
		fooBarReviewsThreeRequest,
		fooBarReviewsTwoRequest,
		fooBarSha1StatusRequest,
		fooBarSha2StatusRequest,
		fooBarSha3StatusRequest,
		fooGuyUserRequest,
		pullsPageOneRequest,
		pullsPageTwoRequest,
	})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	repoFlag := cli.StringSlice{"foofoofoofoofoofoo/bar"}
	set.Var(&repoFlag, "repo", "doc")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:4])
	assert.Equal(
		t,
		[]string{
			"Repo  |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar|1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar|2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar|3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"Total 3",
			"",
		},
		output,
	)
}

func TestCmdParseRepoFilterMultiple(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	repoFlag := cli.StringSlice{"foofoofoofoofoofoo/bar", "own/reprepreprepreprep"}
	set.Var(&repoFlag, "repo", "doc")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	app := cli.NewApp()
	err := command.CmdParse(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdParseNoProfile(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := flag.NewFlagSet("test", 0)
	set.String("config", configFileName, "doc")
	set.String("profile", "bar", "doc")
	app := cli.NewApp()
	err := command.CmdParse(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "Invalid Profile: bar")
}

func TestCmdParseUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"foo"}))
	app := cli.NewApp()
	err := command.CmdParse(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, "Usage: \"prp parse\"")
}

func TestCmdParseBadApiUrl(t *testing.T) {
	_, configFileName := getConfigWithAPIURL(t, "%s/mockApi")
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app := cli.NewApp()
	err := command.CmdParse(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, `parse "%s/mockApi/": invalid URL escape "%s/"`)
}

func TestCmdParseUserFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			mockServer.NewRequest("/user", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app := cli.NewApp()
	err := command.CmdParse(cli.NewContext(app, set, nil))
	assert.EqualError(t, err, fmt.Sprintf("GET %s/user: 500  []", ts.URL))
}

func TestCmdParsePullRequestFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarPullsRequest,
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep/pulls?per_page=100", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:4])
	assert.Equal(
		t,
		[]string{
			"Repo  |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar|1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar|2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar|3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"Total 3",
			"",
		},
		output,
	)
}

func TestCmdParseStatusFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha2StatusRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep/commits/sha1/statuses", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |        |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseLabelFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep/issues/1/labels", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |       |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseCommentFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep/issues/1/comments?per_page=100", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |1 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseCommitCompareFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/compare/fooLabel...fooBaseLabel1", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |N  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseReviewFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/pulls/1/reviews?per_page=100", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|3 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseJira(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	jiraTestServer := getTestJiraServer(t)
	defer jiraTestServer.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.String("jiraUrl", jiraTestServer.URL, "")
	set.String("jiraUsername", "un", "")
	set.String("jiraPassword", "pw", "")
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	defer s.Close()

	set.String("redisAddress", s.Addr(), "")
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira   |Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |foo-1  |Pass  |Looooo N|htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |       |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |       |      |        |htmlUrl",
			"ow/repreprepreprep|1 |prOne     |guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |ABC-123|Review|Dude    |htmlUrl",
			"ow/repreprepreprep|2 |Pull Reque|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |Rea-1  |Open  |Guy     |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdYourls(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	yourlsTestServer := mockServer.New(
		t,
		[]*mockServer.Request{yourlsRequest},
	)
	defer yourlsTestServer.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.String("yourlsUrl", yourlsTestServer.URL, "")

	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |shortUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |shortUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |shortUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |shortUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |shortUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdYourlsRequestError(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	yourlsTestServer := mockServer.New(
		t,
		[]*mockServer.Request{yourlsRequestError},
	)
	defer yourlsTestServer.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.String("yourlsUrl", yourlsTestServer.URL, "")

	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdYourlsRequestBadJson(t *testing.T) {
	ts := getParseTestServerMock(t)
	defer ts.Close()
	yourlsTestServer := mockServer.New(
		t,
		[]*mockServer.Request{yourlsRequestBadJson},
	)
	defer yourlsTestServer.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	set.String("yourlsUrl", yourlsTestServer.URL, "")

	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |boo,baz  |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCmdParseReviewerFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
			mockServer.NewRequest("/repos/foofoofoofoofoofoo/bar/pulls/1/requested_reviewers", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	assert.Nil(t, command.CmdParse(cli.NewContext(app, set, nil)))
	output := strings.Split(writer.String(), "\n")
	sort.Strings(output[1:6])
	assert.Equal(
		t,
		[]string{
			"Repo              |ID|Title     |Owner|Branch|Target     |+1|UTD|Status  |Review|Labels |Reviewers|Jira|Status|Assigned|Link",
			"fo/bar            |1 |fooPrOne  |fooGu|fooRef|fooBaseRef1|5 |Y  |1Y/1N/1?|N     |L,L    |         |    |      |        |htmlUrl",
			"fo/bar            |2 |fooPrTwo  |fooGu|fooRef|fooBaseRef2|1 |N  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"fo/bar            |3 |fooPrThree|fooGu|fooRef|fooBaseRef3|4 |Y  |        |N     |L,L,RLL|foo      |    |      |        |htmlUrl",
			"ow/repreprepreprep|1 |ABC-123: p|guy  |ref1  |baseRef1   |2 |N  |1Y      |N     |L      |foo,bar  |    |      |        |htmlUrl",
			"ow/repreprepreprep|2 |ReallyLong|guy2 |ref2  |baseRef2   |2 |Y  |1N      |Y     |       |goo      |    |      |        |htmlUrl",
			"Total 5",
			"",
		},
		output,
	)
}

func TestCompleteParseFlags(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	app.Commands = []cli.Command{
		{
			Name: "parse",
			Flags: []cli.Flag{
				cli.StringFlag{Name: "owner, o"},
				cli.StringFlag{Name: "repo, r"},
				cli.BoolFlag{Name: "need-rebase, nr"},
				cli.BoolFlag{Name: "verbose, v"},
			},
		},
	}
	os.Args = []string{"parse", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "--owner\n--repo\n--need-rebase\n--verbose\n", writer.String())
}

func TestCompleteParseUser(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{
		fooBarPullsRequest,
		pullsPageOneRequest,
		pullsPageTwoRequest,
	})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--user", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "fooGuy\nguy\nguy2\n", writer.String())
}

func TestCompleteParseUserNoConfig(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := flag.NewFlagSet("test", 0)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--user", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteParseUserBadApiUrl(t *testing.T) {
	_, configFileName := getConfigWithAPIURL(t, "%s/mockApi")
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--user", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteParseUserPullRequestFailure(t *testing.T) {
	ts := mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarPullsRequest,
			mockServer.NewRequest("/repos/own/reprepreprepreprep/pulls?per_page=100", true).WithStatusCode(500),
		},
	)
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--user", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "fooGuy\n", writer.String())
}

func TestCompleteParseRepo(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--repo", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo/bar\nown/reprepreprepreprep\n", writer.String())
}

func TestCompleteParseRepoMulti(t *testing.T) {
	ts := mockServer.New(t, []*mockServer.Request{})
	defer ts.Close()
	_, configFileName := getConfigWithAPIURL(t, ts.URL)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	repoFlag := cli.StringSlice{"foofoofoofoofoofoo/bar"}
	set.Var(&repoFlag, "repo", "doc")
	app, writer, _ := appWithTestWriters()
	os.Args = []string{"parse", "--repo", "foofoofoofoofoofoo/bar", "--repo", "--completion"}
	command.CompleteParse(cli.NewContext(app, set, nil))
	assert.Equal(t, "own/reprepreprepreprep\n", writer.String())
}

func getParseTestServerMock(t *testing.T) *mockServer.Server {
	return mockServer.New(
		t,
		[]*mockServer.Request{
			fooBarCommentRequest,
			fooBarCommentThreeRequest,
			fooBarCommentTwoRequest,
			fooBarCompareOneRequest,
			fooBarCompareThreeRequest,
			fooBarCompareTwoRequest,
			fooBarLabelsRequest,
			fooBarLabelsThreeRequest,
			fooBarLabelsTwoRequest,
			fooBarPullsRequest,
			fooBarReviewersOneRequest,
			fooBarReviewersThreeRequest,
			fooBarReviewersTwoRequest,
			fooBarReviewsOneRequest,
			fooBarReviewsThreeRequest,
			fooBarReviewsTwoRequest,
			fooBarSha1StatusRequest,
			fooBarSha2StatusRequest,
			fooBarSha3StatusRequest,
			fooGuyUserRequest,
			ownRepCommentPageOneRequest,
			ownRepCommentPageTwoRequest,
			ownRepCompareOneRequest,
			ownRepCompareTwoRequest,
			ownRepLabelsRequest,
			ownRepLabelsTwoRequest,
			ownRepReviewersOneRequest,
			ownRepReviewersTwoRequest,
			ownRepReviewsPageOneRequest,
			ownRepReviewsPageTwoRequest,
			ownRepReviewsTwoRequest,
			ownRepSha1StatusRequest,
			ownRepSha2StatusRequest,
			ownRepTwoCommentRequest,
			pullsPageOneRequest,
			pullsPageTwoRequest,
		},
	)
}

func getTestJiraServer(t *testing.T) *mockServer.Server {
	return mockServer.New(
		t,
		[]*mockServer.Request{
			jiraSessionRequest,
			jiraIssueAbc123Request,
			jiraIssueFooRefRequest,
			jiraIssueReallyLongRequest,
		},
	)
}

func getTestYourlsServer(t *testing.T) *mockServer.Server {
	return mockServer.New(
		t,
		[]*mockServer.Request{yourlsRequest},
	)
}
