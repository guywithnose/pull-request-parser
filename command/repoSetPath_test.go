package command_test

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli"
	"gitlab.com/guywithnose/pull-request-parser/command"
)

func TestCmdRepoSetPath(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	repoDir, err := ioutil.TempDir("", "repo")
	assert.Nil(t, err)
	assert.Nil(t, os.MkdirAll(fmt.Sprintf("%s/.git", repoDir), 0777))
	defer removeFile(t, repoDir)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", repoDir}))
	assert.Nil(t, command.CmdRepoSetPath(cli.NewContext(nil, set, nil)))

	expectedConfigFile, disposableConfigFile := getConfigWithTwoRepos(t)
	removeFile(t, disposableConfigFile)
	profile := expectedConfigFile.Profiles["foo"]
	profile.TrackedRepos[1].LocalPath = repoDir
	expectedConfigFile.Profiles["foo"] = profile
	assertConfigFile(t, expectedConfigFile, configFileName)
}

func TestCmdRepoSetPathInvalidPath(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "/notadir"}))
	err := command.CmdRepoSetPath(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "path does not exist: /notadir")
}

func TestCmdRepoSetPathNotGit(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	repoDir, err := ioutil.TempDir("", "repo")
	assert.Nil(t, err)
	defer removeFile(t, repoDir)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", repoDir}))
	err = command.CmdRepoSetPath(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, fmt.Sprintf("path is not a git repo: %s", repoDir))
}

func TestCmdRepoSetPathNoConfig(t *testing.T) {
	err := command.CmdRepoSetPath(cli.NewContext(nil, flag.NewFlagSet("test", 0), nil))
	assert.EqualError(t, err, "You must specify a config file")
}

func TestCmdRepoSetPathInvalidRepo(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "goo"}))

	err := command.CmdRepoSetPath(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Not a valid Repo: own/reprepreprepreprep")
}

func TestCmdRepoSetPathUsage(t *testing.T) {
	_, configFileName := getConfigWithFooProfile(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	err := command.CmdRepoSetPath(cli.NewContext(nil, set, nil))
	assert.EqualError(t, err, "Usage: \"prp profile repo set-path {repoName} {localPath}\"")
}

func TestCompleteRepoSetPathRepos(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	os.Args = []string{"repo", "ignore-build", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoSetPath(cli.NewContext(app, set, nil))
	assert.Equal(t, "foofoofoofoofoofoo/bar\nown/reprepreprepreprep\n", writer.String())
}

func TestCompleteRepoSetPathFileCompletion(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep"}))
	os.Args = []string{"repo", "set-path", "own/reprepreprepreprep", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoSetPath(cli.NewContext(app, set, nil))
	assert.Equal(t, "fileCompletion\n", writer.String())
}

func TestCompleteRepoSetPathDone(t *testing.T) {
	_, configFileName := getConfigWithTwoRepos(t)
	defer removeFile(t, configFileName)
	set := getBaseFlagSet(configFileName)
	assert.Nil(t, set.Parse([]string{"own/reprepreprepreprep", "goo"}))
	os.Args = []string{"repo", "ignore-build", "own/reprepreprepreprep", "goo", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoSetPath(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}

func TestCompleteRepoSetPathNoConfig(t *testing.T) {
	set := flag.NewFlagSet("test", 0)
	os.Args = []string{"repo", "ignore-build", "--completion"}
	app, writer, _ := appWithTestWriters()
	command.CompleteRepoSetPath(cli.NewContext(app, set, nil))
	assert.Equal(t, "", writer.String())
}
