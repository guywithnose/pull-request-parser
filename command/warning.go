package command

type warning interface {
	Warning() bool
}

// IsWarning returns true if err is a warning.
func IsWarning(err error) bool {
	w, ok := err.(warning)
	return ok && w.Warning()
}
