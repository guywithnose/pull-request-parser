package command

import (
	"fmt"
	"sort"
	"strings"

	"github.com/urfave/cli"
)

// CmdRepoUseMerge sets a repo to use merge instead of rebase
func CmdRepoUseMerge(c *cli.Context) error {
	configData, profileName, err := loadProfile(c)
	if err != nil {
		return err
	}

	if c.NArg() != 1 {
		return cli.NewExitError("Usage: \"prp profile repo use-merge {repoName}\"", 1)
	}

	repoName := c.Args().Get(0)

	profile := configData.Profiles[*profileName]
	repo, repoIndex, err := loadRepo(&profile, repoName)
	if err != nil {
		return err
	}

	if repo.MergeInsteadOfRebase {
		return cli.NewExitError(fmt.Sprintf("%s already uses merge instead of rebase", repoName), 1)
	}

	repo.MergeInsteadOfRebase = true
	profile.TrackedRepos[repoIndex] = *repo
	configData.Profiles[*profileName] = profile

	return configData.Write(c.GlobalString("config"))
}

// CompleteRepoUseMerge handles bash autocompletion for the 'profile repo use-merge' command
func CompleteRepoUseMerge(c *cli.Context) {
	if c.NArg() >= 1 {
		return
	}

	configData, profileName, err := loadProfile(c)
	if err != nil {
		return
	}

	profile := configData.Profiles[*profileName]

	repoNames := []string{}
	for _, repo := range profile.TrackedRepos {
		if !repo.MergeInsteadOfRebase {
			repoNames = append(repoNames, fmt.Sprintf("%s/%s", repo.Owner, repo.Name))
		}
	}

	sort.Strings(repoNames)

	fmt.Fprintln(c.App.Writer, strings.Join(repoNames, "\n"))
}
