package mockServer

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// RequestFunc is a custom callable that is called when a matching request is called
type RequestFunc func(*testing.T, http.ResponseWriter, *http.Request, []byte, *Server)

// Server encapsulates an httptest server that expects some mock requests
type Server struct {
	*httptest.Server
	requests []*Request
	t        *testing.T
}

// Request represents an expected request and how we should handle it
type Request struct {
	urlContains        string
	urlMatches         string
	isDefault          bool
	isOptional         bool
	wasRequested       bool
	allowMultipleCalls bool
	extraStep          RequestFunc
	statusCode         int
	response           interface{}
}

// NewRequest returns a new Request that matches if the url matches the specified string
func NewRequest(url string, exactMatch bool) *Request {
	if exactMatch {
		return &Request{urlMatches: url}
	}

	return &Request{urlContains: url}
}

func (ms *Server) verifyRequests() {
	for _, request := range ms.requests {
		if !request.wasRequested && !request.isOptional {
			if request.urlContains != "" {
				assert.Failf(ms.t, "Request never called", "Request containing %s was never called", request.urlContains)
			} else {
				assert.Failf(ms.t, "Request never called", "Request matching %s was never called", request.urlMatches)
			}
		}
	}
}

// Close shuts down the server and blocks until all outstanding requests on
// this server have completed.  It also verifies that all expected requests
// have been run.
func (ms *Server) Close() {
	ms.verifyRequests()
	ms.Server.Close()
}

// SetDefault sets the request to match any request
func (mr *Request) SetDefault() *Request {
	mr.isDefault = true
	return mr
}

// WithExtraStep adds an extra function to be executed on the request
func (mr *Request) WithExtraStep(f RequestFunc) *Request {
	mr.extraStep = f
	return mr
}

// WithResponse sets the response to be returned for the request
func (mr *Request) WithResponse(response interface{}) *Request {
	mr.response = response
	return mr
}

// WithStatusCode sets the status code to return for the request
func (mr *Request) WithStatusCode(statusCode int) *Request {
	mr.statusCode = statusCode
	return mr
}

// WithMulipleCalls allows a request to be called multiple times
func (mr *Request) WithMulipleCalls() *Request {
	mr.allowMultipleCalls = true
	return mr
}

// New creates a new mock server
func New(t *testing.T, requests []*Request) *Server {
	for _, request := range requests {
		request.wasRequested = false
	}

	var s *Server

	s = &Server{
		Server: httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			b, err := ioutil.ReadAll(r.Body)
			assert.Nil(t, err)
			r.Body = ioutil.NopCloser(bytes.NewBuffer(b))

			for _, request := range requests {
				handled := s.handleRequest(t, request, w, r, b)
				if handled {
					return
				}
			}

			assert.Failf(t, "Unexpected request", "No request matched %s", r.URL.String())
		})),
		requests: requests,
		t:        t,
	}

	return s
}

func (ms *Server) handleRequest(t *testing.T, request *Request, w http.ResponseWriter, r *http.Request, b []byte) bool {
	if request.matches(r) {
		request.wasRequested = true
		if request.extraStep != nil {
			request.extraStep(t, w, r, b, ms)
		}

		if request.statusCode != 0 {
			w.WriteHeader(request.statusCode)
		}

		if request.response != nil {
			writeResponse(t, w, request.response)
		}

		return true
	}

	return false
}

func (mr *Request) matches(r *http.Request) bool {
	isCallable := (!mr.wasRequested || mr.allowMultipleCalls)
	matchesContains := (mr.urlContains != "" && strings.Contains(r.URL.String(), mr.urlContains))
	matchesExact := (mr.urlMatches != "" && r.URL.String() == mr.urlMatches)
	return isCallable && (matchesContains || matchesExact || mr.isDefault)
}

func writeResponse(t *testing.T, w io.Writer, response interface{}) {
	switch resp := response.(type) {
	case string:
		_, err := w.Write([]byte(resp))
		assert.Nil(t, err)
	case []byte:
		_, err := w.Write(resp)
		assert.Nil(t, err)
	case interface{}:
		bytes, err := json.Marshal(response)
		assert.Nil(t, err)
		_, err = w.Write(bytes)
		assert.Nil(t, err)
	}
}
